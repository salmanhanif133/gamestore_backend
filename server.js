const express = require('express');
const bodyParser = require('body-parser')
const app = express();
var cors = require('cors')
app.use(cors())

const adProps = {
  ad: [
    {
      id: 0, name: "Fifa20", price: "4000",
      description: {
        short: "Awesome game",
        long: "This game is really great, words cant express how much i love this game. Please buy it for godssake"
      }, category: "Play station 4",
      location: {
        country: "Pakistan",
        province: "Sindh",
        city: "Karachi",
        area: "Gizri"
      },
      time: "today", condition: "Used", isFeatured: true
    },

    {
      id: 1, name: "Fifa 19", price: "2000",
      description: {
        short: "Nice game",
        long: "This game is really great, words cant express how much i love this game. Please buy it for godssake"
      }, category: "Play station 3",
      location: {
        country: "Pakistan",
        province: "Sindh",
        city: "Karachi",
        area: "Gizri"
      },
      time: "today", condition: "Used", isFeatured: false
    },

    {
      id: 2, name: "PES", price: "3000",
      description: {
        short: "great game",
        long: "This game is really great, words cant express how much i love this game. Please buy it for godssake"
      }, category: "Play station 2",
      location: {
        country: "Pakistan",
        province: "Sindh",
        city: "Karachi",
        area: "Gizri"
      },
      time: "today", condition: "Used", isFeatured: true
    },

    {
      id: 3, name: "Cricket", price: "1000",
      description: {
        short: "slightly used game",
        long: "This game is really great, words cant express how much i love this game. Please buy it for godssake"
      }, category: "Xbox 360",
      location: {
        country: "Pakistan",
        province: "Sindh",
        city: "Karachi",
        area: "Gizri"
      },
      time: "today", condition: "Used", isFeatured: true
    },

    {
      id: 4, name: "Naruto", price: "1500",
      description: {
        short: "brand new game",
        long: "This game is really great, words cant express how much i love this game. Please buy it for godssake"
      }, category: "Xbox one",
      location: {
        country: "Pakistan",
        province: "Sindh",
        city: "Karachi",
        area: "Gizri"
      },
      time: "today", condition: "Used", isFeatured: false
    },

    {
      id: 5, name: "SOmething", price: "2500",
      description: {
        short: "I dont like this game",
        long: "This game is really great, words cant express how much i love this game. Please buy it for godssake"
      }, category: "Xbox one s",
      location: {
        country: "Pakistan",
        province: "Sindh",
        city: "Karachi",
        area: "Gizri"
      },
      time: "today", condition: "Used", isFeatured: false
    }

  ]
}


app.use(bodyParser.json());
// support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/api/ad', (req, res, next) => {
  res.send(adProps)
})

app.get('/api/ad/:id', (req, res, next) => {
  res.send(adProps.ad.find(c => c.id === parseInt(req.params['id'])))
})

const port = process.env.PORT || 8000

app.listen(port, () => console.log(`Listening on port ${port}`))